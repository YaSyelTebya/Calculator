# Calculator App

This Android application is a simple calculator that provides three different implementations using different layout approaches. The three layout approaches used are FrameLayout, LinearLayout, and ConstraintLayout. Each layout approach provides the same design for the calculator.

## Features
Three different implementations of a calculator using different layout approaches
Basic arithmetic operations: addition, subtraction, multiplication, and division
Clear button to reset the calculator
Display area to show the entered numbers and the result

## Usage
Clone the repository to your local machine.
Open the project in Android Studio.
Build and run the application on an Android device or emulator.
On the main screen, you will see three buttons representing the different layout approaches.
Click on any of the buttons to open the calculator implementation using the corresponding layout approach.
Use the calculator to perform basic arithmetic operations.
Click the clear button to reset the calculator.