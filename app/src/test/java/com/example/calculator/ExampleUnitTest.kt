package com.example.calculator

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    private lateinit var calculatorRepository: CalculatorRepository

    @Before
    fun initRepository() {
        calculatorRepository = CalculatorRepository()
    }

    @Test
    fun `after changing listener receives updated expression`() {
        calculatorRepository.subscribeToChanges {updatedExpression ->
            assertEquals("7", updatedExpression)
        }
        calculatorRepository.addToExpression("7")
    }

    @Test
    fun `reset sets expression to its initial value`() {
        calculatorRepository.addToExpression("7")

        calculatorRepository.subscribeToChanges {updatedExpression ->
            assertEquals(CalculatorRepository.INITIAL_EXPRESSION, updatedExpression)
        }

        calculatorRepository.reset()
    }

    @Test
    fun `after reset subscriber is forgotten`() {
        calculatorRepository.subscribeToChanges { assert(false) }
        calculatorRepository.reset()
        calculatorRepository.addToExpression("148")
    }
}