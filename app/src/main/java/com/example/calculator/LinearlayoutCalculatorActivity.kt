package com.example.calculator

import android.os.Bundle
import com.example.calculator.databinding.LinearLayoutCalculatorBinding

class LinearlayoutCalculatorActivity(
    buttonSetuper: IButtonSetuper = AppendButtonTextButtonSetuper()
) : CalculatorActivity(), IButtonSetuper by buttonSetuper
{
    private val binding: LinearLayoutCalculatorBinding by lazy {
        LinearLayoutCalculatorBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupInput()
        calculatorRepository.subscribeToChanges { updatedExpression ->
            binding.linearLayoutEnteredMathematicalExpression.text = updatedExpression
        }
    }

    private fun setupInput() {
        with(binding) {
            val buttons = listOf(
                linearButtonDigit1,
                linearButtonDigit2,
                linearButtonDigit3,
                linearButtonDigit4,
                linearButtonDigit5,
                linearButtonDigit6,
                linearButtonDigit7,
                linearButtonDigit8,
                linearButtonDigit9,
                linearButtonOperatorMinus,
                linearButtonOperatorPlus,
                linearButtonOperatorMultiply,
            )
            setupButtons(buttons, calculatorRepository)
        }
    }
}