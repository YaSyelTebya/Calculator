package com.example.calculator

import android.widget.Button

interface IButtonSetuper {
    fun setupButtons(buttons: List<Button>, calculatorRepository: CalculatorRepository)
}