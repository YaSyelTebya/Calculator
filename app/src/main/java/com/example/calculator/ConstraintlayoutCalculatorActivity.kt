package com.example.calculator

import android.os.Bundle
import com.example.calculator.databinding.ConstraintLayoutCalculatorBinding

class ConstraintlayoutCalculatorActivity(
    buttonSetuper: IButtonSetuper = AppendButtonTextButtonSetuper()
) : CalculatorActivity(), IButtonSetuper by buttonSetuper
{
    private val binding: ConstraintLayoutCalculatorBinding by lazy {
        ConstraintLayoutCalculatorBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupInput()
        calculatorRepository.subscribeToChanges { updatedExpression ->
            binding.constraintLayoutEnteredMathematicalExpression.text = updatedExpression
        }
    }

    private fun setupInput() {
        with(binding) {
            val buttons = listOf(
                constraintButtonDigit1,
                constraintButtonDigit2,
                constraintButtonDigit3,
                constraintButtonDigit4,
                constraintButtonDigit5,
                constraintButtonDigit6,
                constraintButtonDigit7,
                constraintButtonDigit8,
                constraintButtonDigit9,
                constraintButtonOperatorMinus,
                constraintButtonOperatorPlus,
                constraintButtonOperatorMultiply,
            )
            setupButtons(buttons, calculatorRepository)
        }
    }
}