package com.example.calculator

import android.os.Bundle
import com.example.calculator.databinding.FrameLayoutCalculatorBinding

class FramelayoutCalculatorActivity(
    buttonSetuper: IButtonSetuper = AppendButtonTextButtonSetuper()
) : CalculatorActivity(), IButtonSetuper by buttonSetuper
{
    private val binding: FrameLayoutCalculatorBinding by lazy {
        FrameLayoutCalculatorBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupInput()
        calculatorRepository.subscribeToChanges { updatedExpression ->
            binding.frameLayoutEnteredMathematicalExpression.text = updatedExpression
        }
    }

    private fun setupInput() {
        with(binding) {
            val buttons = listOf(
                frameButtonDigit1,
                frameButtonDigit2,
                frameButtonDigit3,
                frameButtonDigit4,
                frameButtonDigit5,
                frameButtonDigit6,
                frameButtonDigit7,
                frameButtonDigit8,
                frameButtonDigit9,
                frameButtonOperatorMinus,
                frameButtonOperatorPlus,
                frameButtonOperatorMultiply,
            )
            setupButtons(buttons, calculatorRepository)
        }
    }
}