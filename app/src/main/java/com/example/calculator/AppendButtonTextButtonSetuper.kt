package com.example.calculator

import android.widget.Button

class AppendButtonTextButtonSetuper : IButtonSetuper{
    override fun setupButtons(buttons: List<Button>, calculatorRepository: CalculatorRepository) {
        for(button in buttons) {
            button.setOnClickListener {
                calculatorRepository.addToExpression(button.text)
            }
        }
    }
}