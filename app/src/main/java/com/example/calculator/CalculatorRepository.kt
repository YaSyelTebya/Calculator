package com.example.calculator

import kotlin.properties.Delegates
import kotlin.reflect.KProperty

class CalculatorRepository {
    private var callOnChange: ((String) -> Unit)? = null

    private var mathematicalExpression: String by Delegates.observable(INITIAL_EXPRESSION,
        this::onChange)

    fun addToExpression(char: CharSequence) {
        mathematicalExpression += char
    }

    fun subscribeToChanges(onChange: (String) -> Unit) {
        callOnChange = onChange
    }

    private fun onChange(_property: KProperty<*>, _oldValue: Any?, newValue: Any?) =
        callOnChange?.invoke(newValue.toString())

    fun reset() {
        callOnChange = null
        mathematicalExpression = INITIAL_EXPRESSION
    }

    companion object {
        const val INITIAL_EXPRESSION = ""
    }
}