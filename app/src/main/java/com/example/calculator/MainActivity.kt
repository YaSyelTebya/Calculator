package com.example.calculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.calculator.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val calculatorRepository by lazy { (application as CalculatorApplication).calculatorRepository }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupButtons()
    }

    override fun onResume() {
        super.onResume()
        calculatorRepository.reset()
    }

    private fun setupButtons() {
        with(binding) {
            gotoFramelayoutCalculator.setOnClickListener {
                moveToCalculatorActivity(FramelayoutCalculatorActivity::class.java)
            }
            gotoLinearlayoutCalculator.setOnClickListener {
                moveToCalculatorActivity(LinearlayoutCalculatorActivity::class.java)
            }
            gotoConstraintlayoutCalculator.setOnClickListener {
                moveToCalculatorActivity(ConstraintlayoutCalculatorActivity::class.java)
            }
        }
    }

    private fun <T : CalculatorActivity> moveToCalculatorActivity(calculatorActivity: Class<T>) {
       val gotoCalculatorActivityIntent = Intent(this, calculatorActivity)
        startActivity(gotoCalculatorActivityIntent)
    }
}