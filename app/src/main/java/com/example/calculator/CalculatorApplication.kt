package com.example.calculator

import android.app.Application

class CalculatorApplication : Application() {
    val calculatorRepository by lazy { CalculatorRepository() }
}