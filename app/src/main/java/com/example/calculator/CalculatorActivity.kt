package com.example.calculator

import androidx.appcompat.app.AppCompatActivity

abstract class CalculatorActivity : AppCompatActivity() {
    protected val calculatorRepository: CalculatorRepository by lazy {
        (application as CalculatorApplication).calculatorRepository
    }
}